/*
 * CSc103 Project 5: Syntax highlighting, part one.
 * See readme.html for details.
 * Please list all references you made use of in order to complete the
 * assignment: your classmates, websites, etc.  Aside from the lecture notes
 * and the book, please list everything.  And remember- citing a source does
 * NOT mean it is okay to COPY THAT SOURCE.  What you submit here **MUST BE
 * YOUR OWN WORK**.
 * References:
 * 	readme.html walked me through it essentially from beginning to end.
 * 	No external source and very little effort was needed. 
 *
 * 	code - Jean Pena
 *
 *
 * Finally, please indicate approximately how many hours you spent on this:
 * #hours: ~1 hour max
 */

#include "fsm.h"
using namespace cppfsm;
#include <vector>
using std::vector;
#include <iostream>
using std::cin;
using std::cout;
using std::endl;

// make sure this function returns the old state.  See the header
// file for details.
int cppfsm::updateState(int& state, char c) {
	// save old state
	int oldState = state;

	//new line always resets state to start state
	if(c == '\n') state = start;
	//else is self explanatory. Based on the current state, if the
	//current character is important, change the state to reflect it
	//
	//From fsm.h:
	//start,   // the start state
	//scanid,  // we are in the middle of scanning an identifier
	//comment, // we are scanning a comment
	//strlit,  // we're scanning a string literal (double quotes: "")
	//readfs,  // just read forward slash while scanning a numeric constant
			   // or identifier, or from the start state.
	//readesc, // just read backslash from strlit state (start of escape seq)
	//scannum, // in the middle of scanning a numeric constant
	//error    // error state; read a messed up numeric or escape sequence.
	else switch (state) {
		case start:
        	if (c == '/') state = readfs;
        	else if (c == '"') state = strlit;
			//INSET returns whether c is in the passed set
			else if (INSET(c, num)) state = scannum;
			else if (INSET(c, alpha)) state = scanid;
			//else if (!INSET(c, iddelim)) state = error;//will this happen?
            break;

		case scanid:
        	if (c == '/') state = readfs;
        	else if (c == '"') state = strlit;
			else if (INSET(c, iddelim)) state = start;
			else if (!INSET(c, ident)) state = scanid;
            break;

		case comment:
            break;

		case strlit:
        	if (c == '"') state = start;
        	else if (c == '\\') state = readesc;
            break;

		case readfs:
        	if (c == '/') state = comment;
        	else if (c == '"') state = strlit;
			else if (INSET(c, num)) state = scannum;
			else if (INSET(c, alpha)) state = scanid;
			//else state = error;
            break;

		case readesc:
        	if (INSET(c, escseq)) state = strlit;
			else state = error;
            break;

		case scannum:
        	if (c == '/') state = readfs;
			else if (INSET(c, iddelim)) state = start;
			else if (!INSET(c, num)) state = error;
            break;

		case error:
            break;
	}
	return oldState;
}
