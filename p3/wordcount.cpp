/*
 * CSc103 Project 3: wordcount++
 * See readme.html for details.
 * Please list all references you made use of in order to complete the
 * assignment: your classmates, websites, etc.  Aside from the lecture notes
 * and the book, please list everything.  And remember- citing a source does
 * NOT mean it is okay to COPY THAT SOURCE.  What you submit here **MUST BE
 * YOUR OWN WORK**.
 * References:
 * 	isspace for checking white space - http://www.cplusplus.com/reference/cctype/isspace/
 * 	pair, the return tye of set::insert - http://www.cplusplus.com/reference/set/set/insert/
 * 									   - http://www.cplusplus.com/reference/utility/pair/
 * 	-Jean Pena
 *
 * Finally, please indicate approximately how many hours you spent on this:
 * beginning 2/28: 1:30
 * "Test passed :D" 2/28: 3:30
 * #hours: ~2
 */

#include <iostream>
using std::cin;
using std::cout;
using std::endl;
#include <string>
using std::string;
#include <set>
using std::set;

// write this function to help you out with the computation.
unsigned long countWords(const string& s, set<string>& wl);

int main()
{
	string line;
	set<string> words, lines;
	unsigned long nlines = 0, nwords = 0, characters = 0, uniLines = 0, uniWords = 0;

	//get each line and for each, increment line counter,
	//check if it's unique and increment unique line counter,
	//add it's number of characters to character count,
	//and count the words with countWords() and add that to word counter
	
	while(getline(cin, line)){
		nlines++;
		nwords += countWords(line, words);
		characters += line.length();
		
		//set::insert returns a pair, whose 'second' is a bool which is
		//true only if the inserted object was inserted(because it's unique)
		if(lines.insert(line).second)uniLines++;
	}
	//add '\n' to character count
	characters += nlines;
	//now find out how many unique words
	uniWords = words.size();

#if 0 
	//or if DEBUG
	for (set<string>::iterator i = words.begin(); i!=words.end(); i++) cout << *i << endl;
#endif
	
	//output results
	cout<<nlines<<'\t'<<nwords<<'\t'<<characters<<'\t'<<uniLines<<'\t'<<uniWords<<endl;
	return 0;
}


unsigned long countWords(const string& s, set<string>& wl)
{
	unsigned long sLength = s.length(), wordCount = 0;
	string theBird = "";
	bool wasSpace = true, thisSpace;
	//iterate through each of s's characters
	for(unsigned long i = 0; i<sLength; i++)
	{
		//isspace returns a non zero if determines that the int casted character is a white space
		thisSpace = isspace((int)s[i]);

		/*
		 *reading character, hit a white space. wasSpace=false, thisSpace=true,
		 * 		increment wordCount, add word to the set, and clear theBird
		 *reading character, hit a character. wasSpace=false, thisSpace=false, copy the character
		 *reading white space, hit a character. wasSpace=true, thisSpace=false, copy the character
		 *reading white space, hit more whitespace. wasSpace=true, thisSpace=true, just increment
		 * 
		 * if(thisSpace)==false, copy character
		 * else increment
		 * if(!wasSpace&&thisSpace||endOfLine)==true, add word
		 */

		//if character should be copied
		if(!thisSpace) theBird += s[i];
		//if theBird is the word
		if(!wasSpace && (thisSpace || i == (sLength-1)))
		{
			wordCount++;
			wl.insert(theBird);
			theBird="";
		}
		//prepare for next iteration
		wasSpace = thisSpace;
	}
	return wordCount;
}
