/*
 * CSc103 Project 4: Triangles
 * See readme.html for details.
 * Please list all references you made use of in order to complete the
 * assignment: your classmates, websites, etc.  Aside from the lecture notes
 * and the book, please list everything.  And remember- citing a source does
 * NOT mean it is okay to COPY THAT SOURCE.  What you submit here **MUST BE
 * YOUR OWN WORK**.
 *
 * References:
 * Heronian Right Triangle - http://en.wikipedia.org/wiki/Heronian_triangle#Properties
 * Almost Isosceles Right Triangle - http://en.wikipedia.org/wiki/Special_right_triangles#Almost-isosceles_Pythagorean_triples
 * code - Jean Pena
 *
 * Finally, please indicate approximately how many hours you spent on this:
 * #hours: ~5
 */

#include "triangles.h" // import the prototypes for our triangle class.
#include <iostream>
using std::cout;
#include <vector>
using std::vector;
#include <algorithm>
using std::sort;

// note the "triangle::" part.  We need to specify the function's
// FULL name to avoid confusion.  Else, the compiler will think we
// are just defining a new function called "perimeter"
unsigned long triangle::perimeter() {
	return s1+s2+s3;
}

unsigned long triangle::area() { //Worst case run time: O(1) 
	// Note: why is it okay to return an integer here?  Recall that
	// all of our triangles have integer sides, and are right triangles...
	
	// I wasn't too sure about integer right tringles having integer area. But Wikipedia says so...

	//"Any right-angled triangle whose sidelengths are a Pythagorean triple is a Heronian triangle,
	// as the side lengths of such a triangle are integers, and its area is also an integer,
	// being half of the product of the two shorter sides of the triangle, at least one of which must be even."
	//-Wikipedia

#if 0
	unsigned long b=0, h=0;
	if(s1<s2)
	{
		b=s1;
		if(s2<s3) h=s2;
		else h=s3;
	}
	else
	{
		b=s2;
		if(s1<s3) h=s1;
		else h=s3;
	}
	return (b*h)/2;
#endif
	//the above code is very English. This is a right triangle, so the hypotenuse is the largest side and the
	//other 2 legs are the base and height. The above is picking the smallest 2 sides and plugging them into
	//A = (1/2)*b*h
	//
	//This ugly return does the same thing as above
	return (s1<s2)?((s2<s3)?((s1*s2)/2):((s1*s3)/2)):((s1<s3)?((s1*s2)/2):((s2*s3)/2));
}

void triangle::print() {
	cout << "[" << s1 << "," << s2 << "," << s3 << "]";
}

bool congruent(triangle t1, triangle t2) { //T(c) = 11. Worst case runtime: O(1)
	//This checks the sides of both triangles and returns whether or not they were all equal.
	
	//A bool array used to mark matched sides. The 4th bool is used to retur whether they all matched.
	bool sidesEQ[4] = {0,0,0,1};
	//yeah... this is ugly.
	unsigned long sides[6] = {t1.s1, t1.s2, t1.s3, t2.s1, t2.s2, t2.s3};

	//iterate through 2nd triangle's sides
	for(int i=3; i<6; i++)
		//compare all sides of 1st triangle to the (i-3)th side of the 2nd triangle
		for(int j=0; j<3; j++)
			//if the jth side of the 1st triangle wasn't already  matched,
			//storing whether it matches the (i-3)th side of the 2nd triangle,
			//if a match was stored, stop this loop
			if(!sidesEQ[j] && (sidesEQ[j] = (sides[i] == sides[j])))
				break; // will break 3 times each after some permutation of 1, 2, and 3 steps. T(constant) = 6

	//AND the results of all the sides. Will only be true if all sides found a match. T(constant) = 3
	for(int i=0; i<3; i++)sidesEQ[3] &= sidesEQ[i];
	return sidesEQ[3];
}

bool similar(triangle t1, triangle t2) { //Worst case runtime: O(1)
	//Store sides in array
	unsigned long sides1[3] = {t1.s1, t1.s2, t1.s3};
	unsigned long sides2[3] = {t2.s1, t2.s2, t2.s3};
	
	//sort arrays
	sort(sides1, sides1+3);
	sort(sides2, sides2+3);

	//if all 3 ratios t1.s_i : t2.s_i are equal.
	double r1 = (double)sides1[0]/sides2[0];
	return ((r1 == ((double)sides1[1]/sides2[1])) && (r1 == ((double)sides1[2]/sides2[2])));
}

vector<triangle> findRightTriangles(unsigned long l, unsigned long h) { //T(h) = O(n^3) + 1
	// find all the right triangles with integer sides,
	// subject to the perimeter bigger than l and less than h
	
	vector<triangle> retval; // storage for return value. T(constant) = 1

	//Since c^2 = a^2 + b^2, a, b, and c cannot all be equal
	//Isosceles right triangles cannot have integer sides -Wikipedia
	//Hypotenuse is the largest side
	//Therefore, an integer right triangle must have sides that at least differ by 1
	//
	//If b = a+1 and c = b+1, c will always be the potential hypotenuse
	//
	//For the upper bound,
	//
	//3a + 3 <= h
	//a + 2b + 1 <= h
	//a + b + c <= h
	//
	//These are the conditions that need to be maintained.
	//
	//Finally, the smallest pythagorean triple is (3,4,5)
	//For lower bounds greater than 12, idk. This is brute force, after all.
	for(unsigned long a=3; (3*a + 3) <= h; a++) //T(h) = O(h^3)
		for(unsigned long b=a+1; (a + 2*b +1) <= h; b++)
			for(unsigned long c=b+1; ((a + b + c) >= l) && ((a + b + c) <= h); c++)
				//if right triangle, store it
				if(c*c == a*a + b*b) retval.push_back(*(new triangle(a,b,c)));
	return retval;
}
