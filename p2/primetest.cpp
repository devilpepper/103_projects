/*
 * CSc103 Project 2: prime numbers.
 * See readme.html for details.
 * Please list all references you made use of in order to complete the
 * assignment: your classmates, websites, etc.  Aside from the lecture notes
 * and the book, please list everything.  And remember- citing a source does
 * NOT mean it is okay to COPY THAT SOURCE.  What you submit here **MUST BE
 * YOUR OWN WORK**.
 *
 * References:
 *
 * Square root primality test: en.wikipedia.org/wiki/Primality_test (Naive methods)
 * while(cin >> n): Professor Skeith
 * code: Jean Pena
 */

#include <iostream>
using std::cin;
using std::cout;
using std::endl;
#include <cmath>

//Quietely wait for integer inputs and test for primality
//If prime, output 1
//If composite, output 0
//If integer not entered, end the program
int main()
{
	//longs for input and square root of input and a boolean for primality
	unsigned long n, sqrtN;
	bool primeDawg;

	//until user enters a non integer or Ctrl+D
	while(cin >> n)
	{
		//true if odd or 2
		primeDawg = (n%2==1 || n==2);
		//store square root of n for us in the loop
		sqrtN = (int)sqrt(n);

		//divide n by all odd numbers, i, such that 3 <= i <= sqrtN, until we know n is composite
		for(unsigned long i=3; primeDawg && i<=sqrtN; i+=2)primeDawg &= (n%i!=0);

		//value of primeDawg is true (1) if the loop ended without any 0 remainders and false (0) otherwise
		cout << primeDawg;
		cout << endl;
	}
	return 0;
}
