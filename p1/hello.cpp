/*
 * CSc103 Project 1: (hello_world++)
 * See readme.html for details.
 * Please list all references you made use of in order to complete the
 * assignment: your classmates, websites, etc.  Aside from the lecture notes
 * and the book, please list everything.  And remember- citing a source does
 * NOT mean it is okay to COPY THAT SOURCE.  What you submit here **MUST BE
 * YOUR OWN WORK**.
 * References:
 * template - from readme.html.(would have used "using namespace std otherwise")
 * greeting - "The Princess Bride"
 * code - Jean Pena
 */

#include <iostream>
using std::cin;
using std::cout;
#include <string>
using std::string;

#include <stdint.h>

int main()
{
	//Ask for user name
	string name, relative;
	uint64_t temp= 1<<63;
	cout<<"Enter your name:\n"<<temp;
	getline(cin, name);
	//Ask for relative name
	cout<<"Enter a relative\n";
	getline(cin, relative);
	//Greet user with a greeting modeled after
	//a famous line from "The Princess Bride"
	cout<<"Hello. My name is "<<name<<". "
		<<"You killed my "<<relative<<". Prepare to die\n";
}
