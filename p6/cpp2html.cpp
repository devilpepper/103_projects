/*
 * CSc103 Project 5: Syntax highlighting, part two.
 * See readme.html for details.
 * Please list all references you made use of in order to complete the
 * assignment: your classmates, websites, etc.  Aside from the lecture notes
 * and the book, please list everything.  And remember- citing a source does
 * NOT mean it is okay to COPY THAT SOURCE.  What you submit here **MUST BE
 * YOUR OWN WORK**.
 * References:
 *
 * Jean Pena. No references used.
 *
 *
 * Finally, please indicate approximately how many hours you spent on this:
 * #hours: ~4hours
 */

#include "fsm.h"
using namespace cppfsm;
#include <iostream>
using std::cin;
using std::cout;
using std::endl;
#include <string>
using std::string;
#include <set>
using std::set;
#include <map>
using std::map;
#include <initializer_list> // for setting up maps without constructors.

#include <fstream>
string processLine(string);//prototype

// enumeration for our highlighting tags:
enum {
	hlstatement,  // used for "if,else,for,while" etc...
	hlcomment,    // for comments
	hlstrlit,     // for string literals
	hlpreproc,    // for preprocessor directives (e.g., #include)
	hltype,       // for datatypes and similar (e.g. int, char, double)
	hlnumeric,    // for numeric literals (e.g. 1234)
	hlescseq,     // for escape sequences
	hlerror,      // for parse errors, like a bad numeric or invalid escape
	hlident       // for other identifiers.  Probably won't use this.
};

// usually global variables are a bad thing, but for simplicity,
// we'll make an exception here.
// initialize our map with the keywords from our list:
map<string, short> hlmap = {
#include "res/keywords.txt"
};
// note: the above is not a very standard use of #include...

// map of highlighting spans:
map<int, string> hlspans = {
	{hlstatement, "<span class='statement'>"},
	{hlcomment, "<span class='comment'>"},
	{hlstrlit, "<span class='strlit'>"},
	{hlpreproc, "<span class='preproc'>"},
	{hltype, "<span class='type'>"},
	{hlnumeric, "<span class='numeric'>"},
	{hlescseq, "<span class='escseq'>"},
	{hlerror, "<span class='error'>"}
};
// note: initializing maps as above requires the -std=c++0x compiler flag,
// as well as #include<initializer_list>.  Very convenient though.
// to save some typing, store a variable for the end of these tags:
string spanend = "</span>";

string translateHTMLReserved(char c) {
	switch (c) {
		case '"':
			return "&quot;";
		case '\'':
			return "&apos;";
		case '&':
			return "&amp;";
		case '<':
			return "&lt;";
		case '>':
			return "&gt;";
		case '\t': // make tabs 4 spaces instead.
			return "&nbsp;&nbsp;&nbsp;&nbsp;";
		default:
			char s[2] = {c,0};
			return s;
	}
}



int main() {
	// It may be helpful to break this down and write
	// a function that processes a single line, which
	// you repeatedly call from main().
	
	string str;
	//converts every line from stdin to html and outputs them to stdout
	while(getline(cin, str)) cout<<processLine(str)<<endl;

	return 0;
}

string processLine(string s)
{
	//storage for return string, current syntax element,
	//and html translation of the current character
	string final = "", current = "", translation;
	int state = start, old_state; 	//newline, state would have been start.
	
	//add a new line to return state to start at the end
	//and a null character for while loop condition
	s = s + "\n\0";
	int i = 0; //s iterator
	while(s[i] != '\0')
	{
		//update state and put old state in old_state
		old_state = cppfsm::updateState(state, s[i]);
		//translate current character to html if needed
		translation = translateHTMLReserved(s[i]);

		//do stuff if the state was just changed
		if(old_state != state)
		{
			switch (old_state){
				//if the state was start, put the old syntax element
				//into the return string and start reading the next one
				case start:
					final += current;
					current = translation;
					break;
				//if the state was comment, wrap the old syntax element
				//in a comment span, put it into the return string
				//and begin reading the next syntax element
				case comment:
					final += hlspans[hlcomment] + current + spanend;
					current = translation;
					break;
				//if the state was scannum, wrap the old syntax element
				//in a numeric span, put it into the return string
				//and begin reading the next syntax element
				case scannum:
					final += hlspans[hlnumeric] + current + spanend;
					current = translation;
					break;
				//if the state was readfs, keep reading the current
				//syntax element if it is a comment
				//Otherwise, just add the '/' to the return string
				//and begin reading the next syntax element
				case readfs:
					if(state == comment) current += translation;
					else
					{
						final += current;
						current = translation;
					}
					break;
				//if the state was strlit, wrap the old syntax element
				//in a string span, put it into the return string.
				//if the new state is readesc, put the new character in current
				//to be handled by the state change from readesc.
				//Otherwise, the current character is '\"' and it belongs in the string span
				case strlit:
					if(state == readesc) 
					{
						final += hlspans[hlstrlit] + current + spanend;
						current = translation;
					}
					else
					{
						final += hlspans[hlstrlit] + current + translation + spanend;
						current = "";
					}
					break;
				//if the old state was readesc, add  the current character to the current
				//syntax element, and if the new state is strlit, wrap it in an escseq span
				//and add it to the return string. Otherwise, let this be handled by error.
				case readesc:
					current += translation;
					if(state == strlit) 
					{
						final += hlspans[hlescseq] + current + spanend;
						current = "";
					}
					break;
				//if the old state was scanid, then the last syntax element is an
				//identifier or a keyword. Look for it in hlmap and if it's there,
				//wrap it in the appropriate span. Otherwise don't wrap it.
				//Then add it to the return string and begin readig the next element
				case scanid:
					if(hlmap.find(current) != hlmap.end())
						final += hlspans[hlmap[current]] + current + spanend;
					else final += current;
					current = translation;
					break;
				//if the old state was error, wrap the last syntax element in an error span
				//the only way to change from error is with new line.
				//If this case is encountered, it definitely is the last case and
				//the current '\n' does not matter.
				case error:
					final += hlspans[error] + current + spanend;
					break;
			}

		}
		//if the state wasn't changed but it is start,
		//we still want to process the whitespaces
		else if(state == start)
		{
			final += current;
			current = translation;
		}
		//if the state wasn't changed and it isn't start,
		//keep reading the current syntax element
		else current += translation;
		i++;
	}
	return final;
}
