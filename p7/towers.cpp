/*
 * CSc103 Project 7: Towers of Hanoi
 * See readme.html for details.
 * Please list all references you made use of in order to complete the
 * assignment: your classmates, websites, etc.  Aside from the lecture notes
 * and the book, please list everything.  And remember- citing a source does
 * NOT mean it is okay to COPY THAT SOURCE.  What you submit here **MUST BE
 * YOUR OWN WORK**.
 * References:
 *  getopt_long() - http://www.gnu.org/software/libc/manual/html_node/Getopt-Long-Options.html#Getopt-Long-Options
 *  ctime - http://www.cplusplus.com/reference/ctime/
 *  cout_redirect - http://stackoverflow.com/a/5419388
 *
 *  code - Jean Pena
 *
 * Finally, please indicate approximately how many hours you spent on this:
 * #hours: ~4 hours + 1.5 days trying linear solution
 */

#include <iostream>
using std::cin;
using std::cout;
using std::endl;
#include <getopt.h> // to parse long arguments.
#include <cstdlib> // for atoi function

#include<ctime> //for clock(), clock_t, and CLOCKS_PER_SEC
#ifdef DEBUG
#include<sstream> //for stringstream to mute cout
#else
using std::cerr;
#endif

int help(const char* s=0);
void towers(int, int, int, bool);
void towers_n(int, int, int, bool);
double testTime(void(int, int, int, bool), int, int, int);
void speedTest(int, int);

#ifdef DEBUG
struct cout_redirect {
    cout_redirect( std::streambuf * new_buffer )
    {
		last_buffer = std::cout.rdbuf();
		cout_buffer = std::cout.rdbuf( new_buffer );
	}
    ~cout_redirect( ) {
        std::cout.rdbuf( cout_buffer );
    }
	void tonew(){
		if(last_buffer != cout_buffer)
			last_buffer = std::cout.rdbuf( last_buffer );
	}
	void tocout(){
		if(last_buffer == cout_buffer)
			last_buffer = std::cout.rdbuf( last_buffer );
	}
private:
    std::streambuf *cout_buffer, *last_buffer;
};
#endif

/* Here's a skeleton main function for processing the arguments. */
int main(int argc, char *argv[]) {
	// define long options
	static struct option long_opts[] = {
		{"start",        required_argument, 0, 's'},
		{"end",          required_argument, 0, 'e'},
		{"num-disks",    required_argument, 0, 'n'},
		{"interactive",  no_argument,       0, 'i'},
		{"verbose",      no_argument,       0, 'v'},
		{"linear",       no_argument,       0, 'l'},
		{"test-speed",   no_argument,       0, 't'},
		{0,0,0,0} // this denotes the end of our options.
	};
	// now process the options:
	char c; // to hold the option
	int opt_index = 0;

	int start = 1, end = 3, disks = -1;
	bool verbose = false, interactive = false,
		 nonRecursive = false, test = false;

	while ((c = getopt_long(argc, argv, "s:e:n:iv", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 's':
				if((start = atoi(optarg)) > 3 || start < 1)
					return help("Start must be peg 1, 2, or 3!!!!");
				break;
			case 'e':
				if((end = atoi(optarg)) > 3 || end < 1)
					return help("End must be peg 1, 2, or 3!!!!");
				break;
			case 'n':
				disks = atoi(optarg);
				break;
			case 'i':
				interactive = true;
				break;
			case 'v':
				verbose = true;
				break;
			case 'l':
				nonRecursive = true;
				break;
			case 't':
				test = true;
				break;
			case '?': // this will catch unknown options.
				// here is where you would yell at the user.
				// although, getopt will already print an error message.
				return help();
		}
	}
	
	if(interactive)
	{
		//get details from user
		cout<<"How many disks?";
		cin>>disks;
		do //make sure pegs exists
		{
			cout<<"From which peg(1,2, or 3)?";
			cin>>start;
		}while(start > 3 || start < 1);
		do
		{
			cout<<"To which peg(1,2, or 3)?";
			cin>>end;
		}while(end > 3 || end < 1);
	}

	
	if(test)
		speedTest(start, end);
	else if(disks > 0)//exit if no num-disks was provided
	{
		if(nonRecursive)
			towers_n(disks, start, end, verbose);
		else 
			towers(disks, start, end, verbose); //solve towers of hanoi
	}
	
	return 0;
}

void towers(int disks, int start, int end, bool verbose)
{
	if(disks>0)
	{
		//reduce it to a smaller problem and then move 1 disk
		//from start the one that isn't start or end 
		towers(disks-1, start, start^end, verbose);
		//output this movement
		if(verbose) cout<<"Move a disk from peg "<<start<<" to peg "<<end<<'\n';
		else cout<<start<<'\t'<<end<<'\n';
		//reduce it to a smaller problem and this time, move 1 disk
		//from not start or end to end 
		towers(disks-1, start^end, end, verbose);
	}
}

int help(const char *s)
{
	cout<<(s ? s : "")
		<<"\nValid Arguments:\n"
		<<"\t-s, --start=#\t\tPeg to move from(1,2,3)\n"
		<<"\t-e, --end=#\t\tPeg to move to(1,2,3)\n"
		<<"\t-n, --num-disks=#\tNumber of disks\n"
		<<"\t-i, --interactive\tProgram asks for required information\n"
		<<"\t-v, --verbose\t\tProgram explains every move\n";
	return 1;
}

#if 1

void towers_n(int disks, int start, int end, bool verbose)
{
	if(disks%2!=0) end ^= start;

	start ^= end; //for loop invariant

	int temp;
	bool inSet;
	//change disks' meaning to 2^n - 1
	disks = (1<<disks) - 1;

	for(int i=1; i<=disks; i++)
	{
		temp = i%4;

		if(temp == 1)//1 and 0 are strange special cases
		{
			temp = i-1;
			inSet = (temp%8 == 0 && (temp/=8) > 0);
			while(inSet && ((temp & 0xF) == 0)) temp /= 16;
			temp &= 0xF;
			inSet &= !(temp == 2 || temp == 6 || temp == 8 ||
					temp == 10 || temp == 14);
			if(inSet)	
				start ^= end;
			else
				{
					temp = start;
					start ^= end;
					end = temp;
				}
		}
		else if(temp == 2)
			end ^= start;
		else if(temp == 3)
			start ^= end;
		else
		{
			temp = i;
			inSet = (temp%8 == 0 && (temp/=8) > 0);
			while(inSet && ((temp & 0xF) == 0)) temp /= 16;
			temp &= 0xF;
			inSet &= !(temp == 2 || temp == 6 || temp == 8 ||
					temp == 10 || temp == 14);
			if(inSet)
				end ^= start;
			else
				{
					temp = start;
					start ^= end;
					end = temp;
				}
		}

		if(verbose) cout<<"Move a disk from peg "<<start<<" to peg "<<end<<'\n';
		else cout<<start<<'\t'<<end<<'\n';
	}
}

void speedTest(int start, int end)
{
#ifdef DEBUG
	cout<<"disks\tRecursive\t\tvs.\t\tNon-recursive\n"
		<<"-----\t---------\t\t---\t\t-------------\n";
	std::stringstream s;
	cout_redirect out(s.rdbuf());
	out.tocout();
	double d;
	for(int i=1; i<=30; i++)
	{

		cout<<i<<"\t";

		out.tonew();
		d = testTime(towers, start, end, i);
		out.tocout();
		cout<<d<<"s\t\t\t\t\t";

		out.tonew();
		d = testTime(towers_n, start, end, i);
		out.tocout();
		cout<<d<<"s\n";
	}
#else
	cerr<<"disks\tRecursive\t\tvs.\t\tNon-recursive\n"
		<<"-----\t---------\t\t---\t\t-------------\n";
	for(int i=1; i<=30; i++)
	{

		cerr<<i<<"\t"<<testTime(towers, start, end, i)<<"s\t\t\t\t\t"
			<<testTime(towers_n, start, end, i)<<"s\n";
	}
#endif
	
}

double testTime(void(*funct)(int, int, int, bool), int start, int end, int numdisks) //pass a function and it's paramenters.
														//tests the amount of time it takes to run towers of hanoi
{
    double tiem = 0.0;
    clock_t startTime; //honestly, I'm not sure what a clock_t is. It's just some data type in ctime to me
            
    for(int i=0; i<1; i++) //run 100 tests
    {
            startTime = clock(); //system time before the function is called
            funct(numdisks, start, end, false); //call the function that was passed
            tiem += (double)(clock() - startTime); //subtract start from the system time after the function runs,
                                      //cast it to double, and divide by some constant that's in ctime.
                                      //temp=number seconds the function took to run
    }
    return tiem/1/CLOCKS_PER_SEC; //divide by 100 to get the average
}

#endif
